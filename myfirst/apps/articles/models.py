import datetime
from django.db import models
from django.utils import timezone

class Article(models.Model):
    article_title = models.TextField("Название статьи")
    article_text = models.TextField("Текст статьи")
    pub_date = models.DateTimeField('Дата публикации')

    def __str__(self):
        return f"{self.article_title}"

    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days=7))

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    autor_name = models.TextField('Имя автора')
    comment_text = models.TextField('Текст коментария')

    def __str__(self):
        return f"{self.autor_name}"

    class Meta:
        verbose_name = 'Коментарий'
        verbose_name_plural = 'Коментарии'