from django.urls import path

from . import views

appname = 'articles'

urlpatterns = [
    path('', views.index, name = 'index'), 
    path('<int:article_id>/', views.detail, name = 'detail'),
    path('<int:article_id>/leave_comment', views.leave_comment, name = 'leave_comment'),
    path('leave_article', views.leave_article, name = 'leave_article')
]