from django.http import Http404, HttpResponseRedirect
from  django.shortcuts import render
from django.urls import reverse

from .models import *

def index(request):
    latest_articles_list = Article.objects.order_by('-pub_date')
    return render(request, 'articles/list.html', {'latest_articles_list':latest_articles_list})

def detail(request, article_id):
    try:
        a = Article.objects.get(id = article_id)
    except:
        raise Http404('Статья не найдена')
    latest_comment_list = a.comment_set.order_by('-id')
    return render(request, 'articles\detail.html', {'article':a, 'latest_comment_list':latest_comment_list})

def leave_comment(request, article_id):
    try:
        a = Article.objects.get(id = article_id)
    except:
        raise Http404('Статья не найдена')
    a.comment_set.create(autor_name = request.POST['name'], comment_text = request.POST['text'])
    return HttpResponseRedirect(reverse('detail', args=(a.id,)))

def leave_article(request):
    a = Article.objects.create(article_title= request.POST['name'], article_text= request.POST['text'], pub_date=timezone.now())
    return HttpResponseRedirect(reverse('index'))